#!/bin/sh

function install_modules() {
  echo Installing $1
  cd $1 && npm ci && cd ..
}

install_modules jest-module
install_modules jest-module-2
install_modules other-module
