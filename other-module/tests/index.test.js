'use strict';

const foo = require('../');

describe('module tests', () => {
    afterAll(() => {
        jest.restoreAllMocks();
    });

    it('should log foo', () => {
        const logSpy = jest.spyOn(console, 'log').mockImplementation(() => {});
        foo();
        expect(logSpy).toHaveBeenCalledWith('foo');
    });
});
