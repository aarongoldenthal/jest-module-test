'use strict';

const fs = require('fs');

const defaultConfig = {
    error: 'this is a test'
};

const configName = './test-config.json';
let config;
if (fs.existsSync(configName)) {
    config = JSON.parse(fs.readFileSync(configName));
} else {
    config = defaultConfig;
}

describe('exportable tests', () => {
    it('should pass', () => {
        expect(true).toStrictEqual(true);
    });

    it(`should throw error "${config.error}"`, () => {
        expect(() => { throw new Error(config.error); }).toThrow(config.error);
    });
});
