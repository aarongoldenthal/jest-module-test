# Jest Module Test

Testing options for publishing reusable `jest` tests as npm packages.

## Background

This example is composed of 3 projects:

- `jest-module`: Package with a set of tests that leverage an optional `test-config.json` configuration file
- `jest-module-2`: Package with a factory function creating tests, which can be passed configuration data on instantiation
- `other-module`: Project with its own test and also imports and executes the other test modules

## Executing Tests

From root folder:

```sh
./setup.sh
cd other-module
npm test
```
