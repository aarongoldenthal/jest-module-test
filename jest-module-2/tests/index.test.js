'use strict';

module.exports = (foo) => {
    describe('exportable instantiated test', () => {
        afterAll(() => {
            jest.restoreAllMocks();
        });

        it(`should log "${foo}"`, () => {
            const logSpy = jest.spyOn(console, 'log').mockImplementation(() => {});
            console.log(foo);
            expect(logSpy).toHaveBeenCalledWith(foo);
        });
    });
}
